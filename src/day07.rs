use std::collections::{HashMap, HashSet};
use std::fs;

use regex::Regex;


fn parse_input() -> Vec<String> {
    let data = fs::read_to_string("day07_input.txt").unwrap();
    let rules = data.trim().split("\n").map(|l| l.to_string()).collect();
    rules
}


fn get_outer_bags(rules: Vec<String>) -> HashSet<String> {
    let mut m = HashMap::new();

    let re = Regex::new(r"\d+ ([a-z]+ [a-z]+) bags?").unwrap();

    for rule in rules {
        let mut rule = rule.split(" bags contain ");
        let description = rule.next().unwrap().to_string();
        let contents = rule.next().unwrap();
        
        if contents == "no other bags." {continue}

        let contents = contents
            .split(", ")
            .map(|content| re.captures(content).unwrap().get(1).unwrap().as_str().to_string());

        for content in contents {
            let containers = m.entry(content).or_insert(HashSet::new());
            containers.insert(description.clone());
        }
    }

    let mut recorded = HashSet::new();
    get_containers(&mut recorded, &m, "shiny gold".to_string());
    recorded
}


fn get_containers(mut recorded: &mut HashSet<String>, m: &HashMap<String, HashSet<String>>, bag: String) -> () {
    let new_containers: Vec<String> = match m.get(&bag) {
        Some(containers) => containers.iter().filter(|c| !recorded.contains(*c)).map(|c| c.to_string()).collect(),
        None => vec![],
    };

    for c in &new_containers {
        recorded.insert(c.clone());
    }
    for c in new_containers {
        get_containers(&mut recorded, &m, c);
    }
}


fn get_contents(rules: Vec<String>) -> usize {
    let mut m: HashMap<String, HashSet<(usize, String)>> = HashMap::new();
    let re = Regex::new(r"(\d+) ([a-z]+ [a-z]+) bags?").unwrap();

    for rule in rules {
        let mut rule = rule.split(" bags contain ");
        let description = rule.next().unwrap().to_string();
        let contents = rule.next().unwrap();

        let contents = match contents {
            "no other bags." => HashSet::new(),
            _ => contents.split(", ").map(|content| {
                    let caps = re.captures(content).unwrap();
                    (
                        caps.get(1).unwrap().as_str().parse().unwrap(),
                        caps.get(2).unwrap().as_str().to_string(),
                    )
                })
                .collect()
        };
        m.insert(description, contents);
    }

    total_contents(&m, "shiny gold".to_string())
        
}


fn total_contents(m: &HashMap<String, HashSet<(usize, String)>>, bag: String) -> usize {
    let mut counter = 0;
    let contents = m.get(&bag).unwrap();
    for content in contents {
        let n = content.0;
        let bag = content.1.clone();
        counter += n;
        counter += n * total_contents(&m, bag);
    }
    counter
}


pub fn part_1() -> usize {
    let rules = parse_input();
    let bags = get_outer_bags(rules);
    bags.len()
}


pub fn part_2() -> usize {
    let rules = parse_input();
    get_contents(rules)
}


#[cfg(test)]
mod test {
    use super::*;

    const DATA: &str = "light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags.";

    #[test]
    fn test_part_1() -> () {
        assert_eq!(
            get_outer_bags(DATA.split("\n").map(|rule| rule.to_string()).collect()).len(),
            4
        )
    }


    #[test]
    fn test_part_2() -> () {

    }
}

