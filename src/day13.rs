use super::shared::{iter_lines, RudolphTheRedNosedResult, SantasLittleError};


fn parse_input() -> RudolphTheRedNosedResult<(u64, Vec<u64>)> {
    let mut lines = iter_lines("day13_input.txt").unwrap();
    let earliest = lines.next().unwrap()?.parse()?;
    let buses = lines
        .next().unwrap()?
        .split(",")
        .filter(|&s| s != "x")
        .map(|s| s.parse().unwrap())
        .collect();
    Ok((earliest, buses))
}


fn parse_input2() -> RudolphTheRedNosedResult<Vec<(u64, u64)>> {
    let mut lines = iter_lines("day13_input.txt").unwrap();
    lines.next();
    let buses = lines.next().unwrap()?;
    Ok(buses
        .split(",")
        .enumerate()
        .filter(|(_i, bus)| *bus != "x")
        .map(|(i, bus)| (i as u64, bus.parse().unwrap()))
        .collect())
}


fn get_next_bus(earliest: u64, buses: Vec<u64>) -> RudolphTheRedNosedResult<(u64, u64)> {
    let next_departures = buses.iter().map(|&bus| (bus, ((earliest - 1)/bus + 1) * bus));
    let next_departure = next_departures.min_by_key(|(_bus, time)| *time).unwrap();
    Ok(next_departure)
}


fn find_next_ordered_timetable(buses: Vec<(u64, u64)>, earliest: u64) -> u64 {
    // Possible method using Chinese Remainder Theorem.
    // Probably worth learning for future.
    // Could be faster.
    // https://www.youtube.com/watch?v=ru7mWZJlRQg
    //
    // let total_prod: u64 = buses.iter().cloned().map(|(_i, bus)| bus).product();
    // println!("{:?}", total_prod);
    // let components: Vec<u64> = buses.iter().cloned().map(|(_i, bus)| total_prod/bus).collect();
    // println!("{:?}", components);
    // let mods: Vec<u64> = components.iter().zip(&buses).map(|(c, (_i, b))| c % b).collect();
    // println!("{:?}", mods);
    // 
    // This bit not correct, need to use Extended Euclidean Algorithm to find
    // modular inverse.
    // let mult: Vec<u64> = buses.iter().map(|(i, bus)| i + bus).collect();
    // println!("{:?}", mult);
    // let components: Vec<u64> = components.iter().zip(mult).map(|(c, m)| c * m).collect();
    // println!("{:?}", components);

    // let t = components.iter().sum();
    // println!("{:?}", t);

    // return t

    // This can be sped up by sorting the list.
    // It will only work if setoff - next_bus.0 + first_bus.0
    // cannot become negative. Other modifications required,
    // see comments below, and make buses parameter mutable.
    // buses.sort_by_key(|(_i, bus)| *bus);
    // buses.reverse();
    let mut bus_iter = buses.iter();
    let first_bus = bus_iter.next().unwrap();
    let mut setoff = match earliest {
        0 => 0,
        _ => ((earliest - 1) / first_bus.1 + 1) * first_bus.1,
    };
    let mut next_bus = bus_iter.next().unwrap();
    let mut increment = first_bus.1;

    loop {
        // Use this version when optimising by sorting buses.
        // if (setoff + next_bus.0 - first_bus.0) % next_bus.1 == 0 {
        if (setoff + next_bus.0) % next_bus.1 == 0 {
            increment *= next_bus.1;
            next_bus = match bus_iter.next() {
                Some(bus) => bus,
                // Use this version when optimising by sorting buses.
                // None => return setoff - first_bus.0,
                None => return setoff,
            };
        }
        else { setoff += increment; }
    }
}


pub fn part_1() -> RudolphTheRedNosedResult<u64>{
    let ret = parse_input()?;
    let earliest = ret.0;
    let buses = ret.1;

    let ret = get_next_bus(earliest, buses)?;
    let bus = ret.0;
    let next = ret.1;
    Ok((next - earliest) * bus)
}


pub fn part_2() -> RudolphTheRedNosedResult<u64> {
    let buses = parse_input2()?;
    Ok(find_next_ordered_timetable(buses, 100000000000000))
}

#[cfg(test)]
mod test {
    use super::*;

    const EARLIEST: u64 = 939;
    const BUSES_LIST: [(&str, u64); 6] = [
        ("7,13,x,x,59,x,31,19", 1068781),
        ("17,x,13,19", 3417),
        ("67,7,59,61", 754018),
        ("67,x,7,59,61", 779210),
        ("67,7,x,59,61", 1261476),
        ("1789,37,47,1889", 1202161486),
    ];

    #[test]
    fn test_part_1() -> RudolphTheRedNosedResult<()> {
        let buses = BUSES_LIST[0].0.split(",").filter(|&s| s != "x").map(|s| s.parse().unwrap()).collect();
        let ret = get_next_bus(EARLIEST, buses)?;
        assert_eq!(ret, (59, 944));
        assert_eq!((ret.1 - EARLIEST) * ret.0, 295);
        Ok(())
    }


    #[test]
    fn test_part_2() -> () {
        for buses in &BUSES_LIST {
            let time = buses.1;
            let buses = buses.0
                .split(",")
                .enumerate()
                .filter(|(_i, bus)| *bus != "x")
                .map(|(i, bus)| (i as u64, bus.parse().unwrap()))
                .collect();
            assert_eq!(find_next_ordered_timetable(buses, 0), time);
        }
    }
}

