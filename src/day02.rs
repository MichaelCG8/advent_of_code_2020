use super::shared;
use regex::Regex;


struct PasswordPolicy {
    low: usize,
    high: usize,
    char: char,
    password: String, 
}
#[derive(Debug, Clone)]
struct PasswordPolicyParseErr;


impl std::str::FromStr for PasswordPolicy {
    type Err = PasswordPolicyParseErr;

    fn from_str(s: &str) -> std::result::Result<Self, <Self as std::str::FromStr>::Err> {
        let re = Regex::new(r"^(?P<low>\d+)-(?P<high>\d+) (?P<char>[a-z]): (?P<password>[a-z]+)").unwrap();
        let caps = re.captures(s).unwrap();
        Ok(PasswordPolicy{
            low: caps.name("low").unwrap().as_str().parse().unwrap(),
            high: caps.name("high").unwrap().as_str().parse().unwrap(),
            char: caps.name("char").unwrap().as_str().chars().nth(0).unwrap(),
            password: caps.name("password").unwrap().as_str().to_string(),
        })
    }
}


fn parse_input() -> Vec<PasswordPolicy> {
    let lines = shared::iter_lines("day02_input.txt").unwrap();
    let passwords = lines.map(|line| line.unwrap().parse().unwrap()).collect();
    passwords
}


fn count_valid_passwords_1(passwords: Vec<PasswordPolicy>) -> u16 {
    let mut valid_passwords = 0;
    for password in passwords.iter() {
        let mut letter_count = 0;
        for c in password.password.chars() {
            if c == password.char {
                letter_count += 1;
            }
        }
        if password.low <= letter_count && letter_count <= password.high {
            valid_passwords += 1;
        }
    }
    valid_passwords
}


fn count_valid_passwords_2(passwords: Vec<PasswordPolicy>) -> u16 {
    let mut valid_passwords = 0;
    for password in passwords.iter() {
        let first_matches = password.password.chars().nth(password.low - 1).unwrap() == password.char;
        let second_matches = password.password.chars().nth(password.high - 1).unwrap() == password.char;
        if first_matches ^ second_matches {
            valid_passwords += 1;
        }
    }
    valid_passwords
}


pub fn part_1() -> u16 {
    let passwords = parse_input();
    count_valid_passwords_1(passwords)
}


pub fn part_2() -> u16 {
    let passwords = parse_input();
    count_valid_passwords_2(passwords)
}


#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_part_1() -> () {
        assert_eq!(count_valid_passwords_1(vec!["1-3 a: abcde".parse().unwrap()]), 1);
        assert_eq!(count_valid_passwords_1(vec!["1-3 b: cdefg".parse().unwrap()]), 0);
        assert_eq!(count_valid_passwords_1(vec!["2-9 c: ccccccccc".parse().unwrap()]), 1);
    }


    #[test]
    fn test_part_2() -> () {
        assert_eq!(count_valid_passwords_2(vec!["1-3 a: abcde".parse().unwrap()]), 1);
        assert_eq!(count_valid_passwords_2(vec!["1-3 b: cdefg".parse().unwrap()]), 0);
        assert_eq!(count_valid_passwords_2(vec!["2-9 c: ccccccccc".parse().unwrap()]), 0);
    }
}

