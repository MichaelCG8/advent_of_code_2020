use super::shared::{iter_lines, RudolphTheRedNosedResult, SantasLittleError};


fn parse_input() -> Vec<u32> {
    let lines = iter_lines("day_input.txt").unwrap();
    let expenses = lines.map(|line| line.unwrap().parse().unwrap()).collect();
    expenses
}


pub fn part_1() -> RudolphTheRedNosedResult<String>{
    parse_input();
    Ok("Not yet implemented".to_string())
}


pub fn part_2() -> RudolphTheRedNosedResult<String> {
    parse_input();
    Ok("Not yet implemented".to_string())
}


#[cfg(test)]
mod test {
    use super::*;

    const DATA: &str = "";

    #[test]
    fn test_part_1() -> () {
        let data = DATA;
    }


    #[test]
    fn test_part_2() -> () {
        let data = DATA;
    }
}

