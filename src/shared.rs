use std::error;
use std::fmt;
use std::fs::File;
use std::io::{self, BufRead};
use std::num;


pub fn iter_lines(path: &str) -> io::Result<io::Lines<io::BufReader<File>>> {
    let file = File::open(path)?;
    Ok(io::BufReader::new(file).lines())
}


pub type RudolphTheRedNosedResult<T> = Result<T, SantasLittleError>;


#[derive(Debug)]
pub enum SantasLittleError {
    Io(io::Error),
    Parse(num::ParseIntError),
    Regex(regex::Error),
    Other(String),
}


impl fmt::Display for SantasLittleError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let s = "Ho, ho, hopeless... ";
        match *self {
            SantasLittleError::Io(ref err) => write!(f, "{} IO error: {}", s, err),
            SantasLittleError::Parse(ref err) => write!(f, "{} Parse error: {}", s, err),
            SantasLittleError::Regex(ref err) => write!(f, "{} Regex error: {}", s, err),
            SantasLittleError::Other(ref string_err) => write!(f, "{} Other error: {}", s, string_err),
        }
    }
}


impl error::Error for SantasLittleError {
    fn description(&self) -> &str {
        match *self {
            SantasLittleError::Io(ref err) => err.description(),
            // TODO: Find out why err.description() isn't used.
            SantasLittleError::Parse(ref err) => error::Error::description(err), // err.description(),
            SantasLittleError::Regex(ref err) => err.description(), // err.description(),
            SantasLittleError::Other(ref string_err) => "str_err",
        }
    }

    fn cause(&self) -> Option<&dyn error::Error> {
        match *self {
            SantasLittleError::Io(ref err) => Some(err),
            SantasLittleError::Parse(ref err) => Some(err),
            SantasLittleError::Regex(ref err) => Some(err),
            SantasLittleError::Other(ref string_err) => None,
        }
    }
}


impl From<io::Error> for SantasLittleError {
   fn from(err: io::Error) -> SantasLittleError {
        SantasLittleError::Io(err)
    }
}


impl From<num::ParseIntError> for SantasLittleError {
    fn from(err: num::ParseIntError) -> SantasLittleError {
        SantasLittleError::Parse(err)
    }
}


impl From<regex::Error> for SantasLittleError {
    fn from(err: regex::Error) -> SantasLittleError {
        SantasLittleError::Regex(err)
    }
}

