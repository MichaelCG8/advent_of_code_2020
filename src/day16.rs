use std::collections::{HashMap, HashSet};
use std::fs;

use regex::Regex;

use super::shared::{iter_lines, RudolphTheRedNosedResult, SantasLittleError};


struct Range {
    low: usize,
    high: usize,
}


impl Range {
    pub fn contains(&self, value: &usize) -> bool {
        self.low <= *value && *value <= self.high
    }
}


struct Rule {
    field: String,
    range1: Range,
    range2: Range,
}


impl Rule {
    pub fn contains(&self, value: &usize) -> bool {
        self.range1.contains(value) || self.range2.contains(value)
    }
}


impl Rule {
    pub fn new(s: &str) -> Rule {
        let re = Regex::new(r"(?P<field>\w* ?\w+): (?P<range1_low>\d+)-(?P<range1_high>\d+) or (?P<range2_low>\d+)-(?P<range2_high>\d+)").unwrap();
        let caps = re.captures(s).unwrap();
        let range1 = Range {
            low: caps.name("range1_low").unwrap().as_str().parse().unwrap(),
            high: caps.name("range1_high").unwrap().as_str().parse().unwrap(),
        };
        let range2 = Range {
            low: caps.name("range2_low").unwrap().as_str().parse().unwrap(),
            high: caps.name("range2_high").unwrap().as_str().parse().unwrap(),
        };
        Rule {
            field: caps.name("field").unwrap().as_str().to_string(),
            range1,
            range2,
        }
    }
}


type Ticket = Vec<usize>;


fn parse_input() -> RudolphTheRedNosedResult<(Vec<Rule>, Ticket, Vec<Ticket>)> {
    parse_input_string(fs::read_to_string("day16_input.txt")?)
}


fn parse_input_string(mut input: String) -> RudolphTheRedNosedResult<(Vec<Rule>, Ticket, Vec<Ticket>)> {
    input = input
        .trim()
        .replace("your ticket:", "")
        .replace("nearby tickets:", "");

    let mut input = input.split("\n\n\n");
    let rules = input.next().unwrap();
    let my_ticket = input.next().unwrap();
    let nearby_tickets = input.next().unwrap();
    
    let rules = rules.split("\n").map(|r| Rule::new(r)).collect();
    let my_ticket = my_ticket.split(",").map(|s| s.parse().unwrap()).collect();
    let nearby_tickets = nearby_tickets
        .split("\n")
        .map(|t| t.split(",").map(|s| s.parse().unwrap()).collect())
        .collect();

    Ok((rules, my_ticket, nearby_tickets))
}


fn value_is_valid(value: &usize, rules: &Vec<Rule>) -> bool {
    rules.iter().any(|rule| rule.contains(value))
}


fn ticket_is_valid(ticket: &Ticket, rules: &Vec<Rule>) -> bool {
    ticket.iter().all(|value| value_is_valid(value, &rules))
}


fn get_error_rate((rules, _my_ticket, nearby_tickets): (Vec<Rule>, Ticket, Vec<Ticket>)) -> usize {
    nearby_tickets
        .iter()
        .map(|ticket| 
             ticket
             .iter()
             .filter(|&value| !value_is_valid(value, &rules))
             .sum::<usize>()
        ).sum()
}


fn identify_fields(valid_tickets: Vec<Ticket>, rules: Vec<Rule>) -> HashMap<String, usize> {
    let mut field_mapping = HashMap::<usize, HashSet<String>>::new();
    for field_num in 0..rules.len() {
        let possible_fields = rules
            .iter()
            .filter(|r| valid_tickets.iter().all(|t| r.contains(&t[field_num])))
            .map(|r| r.field.clone())
            .collect();
        field_mapping.insert(field_num, possible_fields);
    }
    let mut final_mapping = HashMap::new();

    loop {
        // Find singular field names.
        for (k, v) in field_mapping.iter_mut() {
            if v.len() == 1 {
                final_mapping.insert(v.iter().next().unwrap().clone(), *k);
            }
        }
        for (v, k) in &final_mapping {
            field_mapping.remove(k);
            for (_k2, v2) in field_mapping.iter_mut() {
                v2.remove(v);
            }
        }
        if field_mapping.len() == 0 {
            return final_mapping
        }
    }
}


fn get_departure_fields_product((rules, my_ticket, nearby_tickets): (Vec<Rule>, Ticket, Vec<Ticket>)) -> usize {
    let valid_tickets: Vec<Ticket> = nearby_tickets.into_iter().filter(|ticket| ticket_is_valid(ticket, &rules)).collect();
    let field_locs = identify_fields(valid_tickets, rules);
    field_locs
        .iter()
        .filter(|(k, _v)| k.starts_with("departure"))
        .map(|(_k, &v)| my_ticket[v])
        .product()
}


pub fn part_1() -> RudolphTheRedNosedResult<usize>{
    let input = parse_input()?;
    Ok(get_error_rate(input))
}


pub fn part_2() -> RudolphTheRedNosedResult<usize> {
    let input = parse_input()?;
    Ok(get_departure_fields_product(input))
}


#[cfg(test)]
mod test {
    use super::*;

    const DATA: &str = "class: 1-3 or 5-7
row: 6-11 or 33-44
seat: 13-40 or 45-50

your ticket:
7,1,14

nearby tickets:
7,3,47
40,4,50
55,2,20
38,6,12";

    #[test]
    fn test_part_1() -> RudolphTheRedNosedResult<()> {
        let data = parse_input_string(DATA.to_string())?;
        assert_eq!(get_error_rate(data), 71);
        Ok(())
    }


    #[test]
    fn test_part_2() -> RudolphTheRedNosedResult<()> {
        let data = parse_input_string(DATA.to_string())?;
        let rules = data.0;
        let valid_tickets = data.2.into_iter().filter(|ticket| ticket_is_valid(ticket, &rules)).collect();
        let mut reference = HashMap::new();
        reference.insert("row".to_string(), 0);
        reference.insert("class".to_string(), 1);
        reference.insert("seat".to_string(), 2);
        assert_eq!(identify_fields(valid_tickets, rules), reference);
        Ok(())
    }
}

