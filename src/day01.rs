use super::shared;


fn parse_input() -> Vec<u32> {
    let lines = shared::iter_lines("day01_input.txt").unwrap();
    let expenses = lines.map(|line| line.unwrap().parse().unwrap()).collect();
    expenses
}


fn find_pair_product(expenses: Vec<u32>) -> u32 {
    for i in 0..expenses.len() {
        for j in i..expenses.len() {
            if expenses[i] + expenses[j] == 2020 {
                return expenses[i] * expenses[j]
            }
        }
    }
    panic!("Did not find a solution");
}


fn find_trio_product(expenses: Vec<u32>) -> u32 {
    for i in 0..expenses.len() {
        for j in i..expenses.len() {
            for k in j..expenses.len() {
                if expenses[i] + expenses[j] + expenses[k] == 2020 {
                    return expenses[i] * expenses[j] * expenses[k]
                }
            }
        }
    }
    panic!("Did not find a solution.");
}


pub fn part_1() -> u32 { 
    let expenses = parse_input();
    find_pair_product(expenses)
}


pub fn part_2() -> u32 {
    let expenses = parse_input();
    find_trio_product(expenses)
}


#[cfg(test)]
mod test {
    use super::*;

    static EXPENSES = vec![1721, 979, 366, 299, 675, 1456];

    #[test]
    fn test_part_1() -> () {
        assert_eq!(find_pair_product(EXPENSES), 514579);
    }


    #[test]
    fn test_part_2() -> () {
        assert_eq!(find_trio_product(EXPENSES), 241861950);
    }
}

