use regex::Regex;

use super::shared::{iter_lines, RudolphTheRedNosedResult, SantasLittleError};


fn parse_input() -> RudolphTheRedNosedResult<Vec<Motion>> {
    let lines = iter_lines("day12_input.txt")?;
    let re = Regex::new(r"(?P<action>[NSEWLRF])(?P<value>\d+)$")?;
    let motions = lines.map(|line| line_to_motion(&re, line.unwrap()).unwrap()).collect();
    Ok(motions)
}


fn line_to_motion(re: &Regex, line: String) -> RudolphTheRedNosedResult<Motion> {
    let caps = re.captures(&line).unwrap();
    Ok(Motion {
        action: caps.name("action").unwrap().as_str().to_string(),
        value: caps.name("value").unwrap().as_str().parse()?,
    })
}


struct Motion {
    action: String,
    value: isize,
}


fn motions_to_manhattan(motions: Vec<Motion>) -> RudolphTheRedNosedResult<isize> {
    let mut lat = 0;
    let mut lon = 0;
    let mut bearing: isize = 90;
    let bearing_map = [
        "N".to_string(), 
        "E".to_string(), 
        "S".to_string(), 
        "W".to_string(),
    ];

    for mut motion in motions {
        if motion.action == "F" {
            motion = Motion { action: bearing_map[(bearing/90) as usize].clone(), value: motion.value };
        }
        match motion {
            Motion { action, value } if action == "N" => lat += value,
            Motion { action, value } if action == "E" => lon += value,
            Motion { action, value } if action == "S" => lat -= value,
            Motion { action, value } if action == "W" => lon -= value,
            Motion { action, value } if action == "L" => bearing = (bearing - value + 360) % 360,
            Motion { action, value } if action == "" => bearing = (bearing + value + 360) % 360,
            _ => return Err(SantasLittleError::Other(format!("Unrecognised action code. {}:{}", file!(), line!()))),
        }
    }
    Ok(lat.abs() + lon.abs())
}


fn motions_to_manhattan2(motions: Vec<Motion>) -> RudolphTheRedNosedResult<isize> {
    let mut lat = 0;
    let mut lon = 0;
    let mut wp_rel_lat = 1;
    let mut wp_rel_lon = 10;

    for motion in motions {
        match motion {
            Motion { action, value } if action == "N" => wp_rel_lat += value,
            Motion { action, value } if action == "E" => wp_rel_lon += value,
            Motion { action, value } if action == "S" => wp_rel_lat -= value,
            Motion { action, value } if action == "W" => wp_rel_lon -= value,
            Motion { action, value } if action == "L" => {
                for _ in 0..(value/90) {
                    let temp = wp_rel_lat;
                    wp_rel_lat = wp_rel_lon;
                    wp_rel_lon = - temp;
                }
            },
            Motion { action, value } if action == "R" => {
                for _ in 0..(value/90) {
                    let temp = wp_rel_lat;
                    wp_rel_lat = -wp_rel_lon;
                    wp_rel_lon = temp;
                }
            },
            Motion { action, value } if action == "F" => { lat += wp_rel_lat * value; lon += wp_rel_lon * value; },
            _ => return Err(SantasLittleError::Other(format!("Unrecognised action code. {}:{}", file!(), line!()))),
        }
    }
    Ok(lat.abs() + lon.abs())
}


pub fn part_1() -> RudolphTheRedNosedResult<isize>{
    let motions = parse_input()?;
    Ok(motions_to_manhattan(motions)?)
}


pub fn part_2() -> RudolphTheRedNosedResult<isize> {
    let motions = parse_input()?;
    Ok(motions_to_manhattan2(motions)?)
}

#[cfg(test)]
mod test {
    use super::*;

    const DATA: &str = "F10
N3
F7
R90
F11";

    #[test]
    fn test_part_1() -> RudolphTheRedNosedResult<()> {
        let re = Regex::new(r"(?P<action>[NSEWLRF])(?P<value>\d+)$").unwrap();
        let motions = DATA.split("\n").map(|line| line_to_motion(&re, line.to_string()).unwrap()).collect();
        assert_eq!(motions_to_manhattan(motions)?, 25);
        Ok(())
    }


    #[test]
    fn test_part_2() -> RudolphTheRedNosedResult<()> {
        let re = Regex::new(r"(?P<action>[NSEWLRF])(?P<value>\d+)$").unwrap();
        let motions = DATA.split("\n").map(|line| line_to_motion(&re, line.to_string()).unwrap()).collect();
        assert_eq!(motions_to_manhattan2(motions)?, 286);
        Ok(())
    }
}

