use super::shared;


fn parse_input() -> Vec<String> {
    let lines = shared::iter_lines("day05_input.txt").unwrap();
    let seats = lines.map(|line| line.unwrap()).collect();
    seats
}


fn seat_to_row_col(seat: String) -> (usize, usize) {
    let row = &seat[..7];
    let row = row.replace("F", "0").replace("B", "1");
    let row = usize::from_str_radix(row.as_str(), 2).unwrap();

    let col = &seat[7..];
    let col = col.replace("L", "0").replace("R", "1");
    let col = usize::from_str_radix(col.as_str(), 2).unwrap();
    (row, col)
}


fn seats_to_seat_ids(seats: Vec<String>) -> Vec<usize> {
    seats.into_iter()
        .map(|seat| seat_to_row_col(seat))
        .map(|(row, col)| row * 8 + col)
        .collect()
}


fn find_seat(seats: Vec<String>) -> usize {
    let rows_cols = seats.into_iter().map(|seat| seat_to_row_col(seat)).collect::<Vec<(usize, usize)>>();
    let maxrow = rows_cols.iter().map(|(r, _c)| r).max().unwrap();
    let minrow = rows_cols.iter().map(|(r, _c)| r).min().unwrap();

    for row in (minrow + 1)..*maxrow {
        for col in 0..6 {
            if !rows_cols.iter().any(|&x| x == (row, col)) {
                return row * 8 + col;
            }
        }
    }
    panic!("Could not identify seat.");
}


pub fn part_1() -> usize {
    let seats = parse_input();
    let seat_ids = seats_to_seat_ids(seats);
    seat_ids.iter().max().unwrap().clone()
}


pub fn part_2() -> usize {
    let seats = parse_input();
    find_seat(seats)
}


#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_part_1() -> () {
        let seats = vec![
            "FBFBBFFRLR",
            "BFFFBBFRRR",
            "FFFBBBFRRR",
            "BBFFBBFRLL",
        ].iter().map(|s| s.to_string()).collect();
        assert_eq!(seats_to_seat_ids(seats), vec![357, 567, 119, 820]);
    }


    #[test]
    fn test_part_2() -> () {
    }
}

