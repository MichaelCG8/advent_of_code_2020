mod shared;
mod day20;

use shared::RudolphTheRedNosedResult;


fn main() -> RudolphTheRedNosedResult<()> {
    println!("Part 1: {}", day20::part_1()?);
    println!("Part 2: {}", day20::part_2()?);
    Ok(())
}

