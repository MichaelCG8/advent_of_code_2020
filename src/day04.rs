use std::fs;
use regex::Regex;


fn parse_input() -> Vec<String> {
    let passports = fs::read_to_string("day04_input.txt")
        .unwrap()
        .split("\n\n")
        .map(|x| x.to_string())
        .collect();
    passports
}


fn fields_present(passport: String) -> bool {
    let fields = vec!["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]; 
    for field in &fields {
        if !passport.contains(field) {
            return false
        }
    }
    true
}


fn filter_fields_present(passports: Vec<String>) -> Vec<String> {
    passports.into_iter().filter(|passport| fields_present(passport.to_string())).collect()
}


fn filter_valid_data(passports: Vec<String>) -> Vec<String> {
    passports.into_iter().filter(|passport| {
        let re = Regex::new(r"byr:(\d{4})(\s|$)").unwrap();
        if re.captures(passport).is_none() { return false }
        let value = re.captures(passport).unwrap().get(1).unwrap().as_str().parse().unwrap();
        if !(1920 <= value && value <= 2002) { return false }

        let re = Regex::new(r"iyr:(\d{4})(\s|$)").unwrap();
        if re.captures(passport).is_none() { return false }
        let value = re.captures(passport).unwrap().get(1).unwrap().as_str().parse().unwrap();
        if !(2010 <= value && value <= 2020) { return false }

        let re = Regex::new(r"eyr:(\d{4})(\s|$)").unwrap();
        if re.captures(passport).is_none() { return false }
        let value = re.captures(passport).unwrap().get(1).unwrap().as_str().parse().unwrap();
        if !(2020 <= value && value <= 2030) { return false }

        let re = Regex::new(r"hgt:(\d{2,3})(cm|in)(\s|$)").unwrap();
        if re.captures(passport).is_none() { return false }
        match re.captures(passport) {
            Some(cap) => {
                let value = cap.get(1).unwrap().as_str().parse().unwrap();
                let units = cap.get(2);
                match units {
                    Some(u) => { 
                        match u.as_str() {
                            "cm" => { if !(150 <= value && value <= 193) { return false } },
                            "in" => { if !(59 <= value && value <= 76) { return false } },
                            _ => return false,
                        }
                    },
                    None => return false,
                }
            },
            None => return false,
        }
        
        let re = Regex::new(r"hcl:#[0-9a-f]{6}(\s|$)").unwrap();
        if re.captures(passport).is_none() { return false }
        if re.captures(passport).is_none() { return false }

        let re = Regex::new(r"ecl:(amb|blu|brn|gry|grn|hzl|oth)(\s|$)").unwrap();
        if re.captures(passport).is_none() { return false }
        if re.captures(passport).is_none() { return false }
        
        let re = Regex::new(r"pid:\d{9}(\s|$)").unwrap();
        if re.captures(passport).is_none() { return false }
        if re.captures(passport).is_none() { return false }

        true
    })
    .collect()
}


fn filter_valid_passports(passports: Vec<String>) -> Vec<String> {
    let mut passports = filter_fields_present(passports);
    passports = filter_valid_data(passports);
    passports
}


pub fn part_1() -> usize {
    let passports = parse_input();
    filter_fields_present(passports).len()
}


pub fn part_2() -> usize {
    let passports = parse_input();
    filter_valid_passports(passports).len()
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn part_1() -> () {
        let passports = "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:1937 iyr:2017 cid:147 hgt:183cm

iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
hcl:#cfa07d byr:1929

hcl:#ae17e1 iyr:2013
eyr:2024
ecl:brn pid:760753108 byr:1931
hgt:179cm

hcl:#cfa07d eyr:2025 pid:166559648
iyr:2011 ecl:brn hgt:59in"
                         .split("\n\n")
                         .map(|x| x.to_string())
                         .collect();
        assert_eq!(filter_fields_present(passports).len(), 2);
    }


    #[test]
    fn part_2() -> () {
        let passports = "eyr:1972 cid:100
hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926
                     
iyr:2019
hcl:#602927 eyr:1967 hgt:170cm
ecl:grn pid:012533040 byr:1946
                     
hcl:dab227 iyr:2012
ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277
                     
hgt:59cm ecl:zzz
eyr:2038 hcl:74454a iyr:2023
pid:3556412378 byr:2007"
                      .split("\n\n")
                         .map(|x| x.to_string())
                         .collect();
        assert_eq!(filter_valid_passports(passports).len(), 0);

        let passports = "pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980
hcl:#623a2f

eyr:2029 ecl:blu cid:129 byr:1989
iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm

hcl:#888785
hgt:164cm byr:2001 iyr:2015 cid:88
pid:545766238 ecl:hzl
eyr:2022

iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719"
                         .split("\n\n")
                         .map(|x| x.to_string())
                         .collect::<Vec<String>>();
        assert_eq!(filter_valid_passports(passports).len(), 4);
    }
}

