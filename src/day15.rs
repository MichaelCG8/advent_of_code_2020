use std::collections::HashMap;
use super::shared::{iter_lines, RudolphTheRedNosedResult, SantasLittleError};


const INPUT: [usize; 6] = [2, 0, 1, 9, 5, 19];


#[derive(Debug)]
struct Entry {
    first: bool,
    last_pos: usize,
    diff: Option<usize>,
}


fn get_nth_number(arr: Vec<usize>, n: usize) -> usize {
    let mut last_hit = HashMap::new();
    for(i, &val) in arr.iter().enumerate() {
        last_hit.insert(val, Entry{first: true, last_pos: i, diff: None} );
    }
    let mut last_value = arr[arr.len() - 1];
    let mut last_entry = last_hit.get(&last_value).unwrap();
    for i in arr.len()..n {
        let new_value = if last_entry.first {0} else {last_entry.diff.unwrap()};
        if last_hit.contains_key(&new_value) {
            let new_entry = last_hit.get_mut(&new_value).unwrap();
            new_entry.first = false;
            new_entry.diff = Some(i - new_entry.last_pos);
            new_entry.last_pos = i;
            last_entry = new_entry;
        }
        else {
            let new_entry = Entry{first: true, last_pos: i, diff: None};
            last_hit.insert(new_value, new_entry);
            last_entry = last_hit.get(&new_value).unwrap();
        }
        last_value = new_value;
    }
    last_value
}


pub fn part_1() -> RudolphTheRedNosedResult<usize>{
    Ok(get_nth_number(INPUT.to_vec(), 2020))
}


pub fn part_2() -> RudolphTheRedNosedResult<usize> {
    Ok(get_nth_number(INPUT.to_vec(), 30000000))
}

#[cfg(test)]
mod test {
    use super::*;

    const DATA: [([usize; 3], usize, usize); 7] = [
        ([0, 3, 6], 436, 175594),
        ([1, 3, 2], 1, 2578),
        ([2, 1, 3], 10, 3544142),
        ([1, 2, 3], 27, 261214),
        ([2, 3, 1], 78, 6895259),
        ([3, 2, 1], 438, 18),
        ([3, 1, 2], 1836, 362),
    ];

    
    #[test]
    fn test_part_1() -> () {
        for (arr, result, _) in DATA.iter() {
            assert_eq!(get_nth_number((*arr).to_vec(), 2020), *result);
        }
    }


    #[test]
    fn test_part_2() -> () {
        for (arr, _, result) in DATA.iter() {
            assert_eq!(get_nth_number((*arr).to_vec(), 30000000), *result);
        }
    }
}

