use regex::Regex;

use super::shared::{iter_lines, RudolphTheRedNosedResult, SantasLittleError};


fn parse_input() -> Vec<String> {
    let lines = iter_lines("day18_input.txt").unwrap();
    lines.map(|line| line.unwrap()).collect()
}


fn solve_sub_question_l2r(question: &str) -> usize {
    let mut parts = question.split(" ");
    let mut result = parts.next().unwrap().parse().unwrap();
    loop {
        let op = match parts.next() {
            Some(op) => op,
            None => break,
        };
        let val: usize = parts.next().unwrap().parse().unwrap();
        match op {
            "*" => result *= val,
            "+" => result += val,
            _ => panic!(),
        };
    }
    result
}


fn solve_sub_question_plus2mult(question: &str) -> usize {
    let re = Regex::new(r"\d+ \+ \d+").unwrap();
    let mut question: String = question.to_string();
    loop {
        let sub_question = re.captures(&question);
        let sub_answer: usize = match sub_question {
            Some(q) => {
                let mut parts = q.get(0).unwrap().as_str().split(" ");
                let a: usize = parts.next().unwrap().parse().unwrap();
                parts.next().unwrap();
                let b: usize = parts.next().unwrap().parse().unwrap();
                a + b
            },
            None => break,
        };
        question = re.replace(&question, format!("{}", sub_answer).as_str()).to_string();
    }

    let re = Regex::new(r"\d+ \* \d+").unwrap();
    loop {
        let sub_question = re.captures(&question);
        let sub_answer: usize = match sub_question {
            Some(q) => {
                let mut parts = q.get(0).unwrap().as_str().split(" ");
                let a: usize = parts.next().unwrap().parse().unwrap();
                parts.next().unwrap();
                let b: usize = parts.next().unwrap().parse().unwrap();
                a * b
            },
            None => break,
        };
        question = re.replace(&question, format!("{}", sub_answer).as_str()).to_string();
    }
    question.parse().unwrap()
}


fn solve_line(mut line: String, solver: &dyn Fn(&str) -> usize) -> usize {
    let re = Regex::new(r"\(\d+( [\+\*] \d+)*\)").unwrap();
    let parentheses: &[_] = &['(', ')'];
    loop {
        let sub_question = re.captures(&line);
        let sub_question = match sub_question {
            Some(q) => q.get(0).unwrap().as_str().trim_matches(parentheses),
            None => break,
        };
        let sub_answer = solver(sub_question);
        line = re.replace(&line, format!("{}", sub_answer).as_str()).to_string();
    }
    solver(&line) 
}


pub fn part_1() -> RudolphTheRedNosedResult<usize>{
    let questions = parse_input();
    Ok(questions.into_iter().map(|q| solve_line(q, &solve_sub_question_l2r)).sum())
}


pub fn part_2() -> RudolphTheRedNosedResult<usize> {
    let questions = parse_input();
    Ok(questions.into_iter().map(|q| solve_line(q, &solve_sub_question_plus2mult)).sum())
}


#[cfg(test)]
mod test {
    use super::*;

    const DATA: [(&str, usize, usize); 6] = [
        ("1 + 2 * 3 + 4 * 5 + 6", 71, 231),
        ("1 + (2 * 3) + (4 * (5 + 6))", 51, 51),
        ("2 * 3 + (4 * 5)", 26, 46),
        ("5 + (8 * 3 + 9 + 3 * 4 * 3)", 437, 1445),
        ("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))", 12240, 669060),
        ("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2", 13632, 23340),
    ];

    #[test]
    fn test_part_1() -> () {
        for (question, answer, _) in DATA.iter() {
            assert_eq!(solve_line(question.to_string(), &solve_sub_question_l2r), *answer);
        }
    }


    #[test]
    fn test_part_2() -> () {
        for (question, _, answer) in DATA.iter() {
            assert_eq!(solve_line(question.to_string(), &solve_sub_question_plus2mult), *answer);
        }
    }
}

