use super::shared;


fn parse_input() -> Vec<Vec<char>> {
    let lines = shared::iter_lines("day03_input.txt").unwrap();
    let tree_map = lines.map(|line| line.unwrap().trim().chars().collect()).collect();
    tree_map
}


fn plot_course(tree_map: &Vec<Vec<char>>, x_step: usize, y_step: usize) -> u64 {
    let mut x_loc = 0;
    let map_width = tree_map[0].len();
    let mut tree_count = 0;
    for (y, line) in tree_map.iter().enumerate() {
        if (y % y_step) > 0 { continue }
        if line[x_loc] == '#' {
            tree_count += 1;
        }
        x_loc = (x_loc + x_step) % map_width;
    }
    tree_count
}


pub fn part_1() -> u64 {
    let tree_map = parse_input();
    let close_trees = plot_course(&tree_map, 3, 1);
    close_trees
}


pub fn part_2() -> u64 {
    let tree_map = parse_input();
    let xy_steps = vec![(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)];
    let tree_product: u64 = xy_steps.iter().map(|(x, y)| plot_course(&tree_map, *x, *y)).product();
    tree_product
}


#[cfg(test)]
mod test {
    use super::*;
    
    #[test]
    fn test_part_1() -> () {
        let tree_map = vec![
            "..##.......",
            "#...#...#..",
            ".#....#..#.",
            "..#.#...#.#",
            ".#...##..#.",
            "..#.##.....",
            ".#.#.#....#",
            ".#........#",
            "#.##...#...",
            "#...##....#",
            ".#..#...#.#",
        ].iter().map(|s| s.chars().collect()).collect();
        assert_eq!(plot_course(&tree_map, 3, 1), 7);
    }


    #[test]
    fn test_part_2() -> () {
        let tree_map = vec![
            "..##.......",
            "#...#...#..",
            ".#....#..#.",
            "..#.#...#.#",
            ".#...##..#.",
            "..#.##.....",
            ".#.#.#....#",
            ".#........#",
            "#.##...#...",
            "#...##....#",
            ".#..#...#.#",
        ].iter().map(|s| s.chars().collect()).collect();
        assert_eq!(plot_course(&tree_map, 1, 1), 2);
        assert_eq!(plot_course(&tree_map, 3, 1), 7);
        assert_eq!(plot_course(&tree_map, 5, 1), 3);
        assert_eq!(plot_course(&tree_map, 7, 1), 4);
        assert_eq!(plot_course(&tree_map, 1, 2), 2);
    }
}
    
