use regex::Regex;

use super::shared::{iter_lines, RudolphTheRedNosedResult};


enum Op {
    NOP,
    ACC,
    JMP,
}

impl From<&str> for Op {
    fn from(s: &str) -> Self {
        match s {
            "nop" => Self::NOP,
            "acc" => Self::ACC,
            "jmp" => Self::JMP,
            _ => panic!(),
        }
    }
}


struct Instruction {
    op: Op,
    val: isize,
}


fn line_to_instruction(line: &str, re: &Regex) -> Instruction {
    let caps = re.captures(line).unwrap();
    Instruction {
        op: Op::from(caps.name("op").unwrap().as_str()),
        val: caps.name("val").unwrap().as_str().parse().unwrap()
    }
}


fn parse_input() -> Vec<Instruction> {
    let lines = iter_lines("day08_input.txt").unwrap();
    let re = Regex::new(r"(?P<op>nop|acc|jmp) (?P<val>[\+\-]\d+)").unwrap();
    let instructions = lines.map(|line| line_to_instruction(&line.unwrap(), &re)).collect();
    instructions
}


fn run_accumulator(instructions: Vec<Instruction>) -> isize {
    let mut visited = vec![false; instructions.len()];

    let mut acc = 0;
    let mut p_inst = 0;
    loop {
        if visited[p_inst] { break }
        visited[p_inst] = true;
        match instructions[p_inst] {
            Instruction {op: Op::NOP, val: _ } => p_inst += 1,
            Instruction {op: Op::ACC, val } => { acc += val; p_inst += 1; },
            Instruction {op: Op::JMP, val } => p_inst = ((p_inst as isize) + val) as usize,
        }
    }
    acc
}


fn run_accumulator_2(instructions: &Vec<Instruction>) -> Option<isize> {
    let mut visited = vec![false; instructions.len()];

    let mut acc = 0;
    let mut p_inst = 0;
    loop {
        if p_inst >= instructions.len() { return Some(acc) }
        if visited[p_inst] { return None }

        visited[p_inst] = true;
        match instructions[p_inst] {
            Instruction {op: Op::NOP, val: _ } => p_inst += 1,
            Instruction {op: Op::ACC, val } => { acc += val; p_inst += 1; },
            Instruction {op: Op::JMP, val } => p_inst = ((p_inst as isize) + val) as usize,
        }
    }
}


fn change_ops(mut instructions: Vec<Instruction>) -> isize {
    for p_inst in 0..instructions.len() {
        match instructions[p_inst].op {
            Op::NOP => {
                instructions[p_inst].op = Op::JMP;
                match run_accumulator_2(&instructions) {
                    Some(acc) => return acc,
                    None => instructions[p_inst].op = Op::NOP,
                }
            },
            Op::ACC => (),
            Op::JMP => {
                instructions[p_inst].op = Op::NOP;
                match run_accumulator_2(&instructions) {
                    Some(acc) => return acc,
                    None => instructions[p_inst].op = Op::JMP,
                }
            },
        }
    }
    panic!();
}


pub fn part_1() -> RudolphTheRedNosedResult<isize> {
    let instructions = parse_input();
    Ok(run_accumulator(instructions))
}


pub fn part_2() -> RudolphTheRedNosedResult<isize> {
    let instructions = parse_input();
    Ok(change_ops(instructions))
}


#[cfg(test)]
mod test {
    use super::*;

    const INSTRUCTIONS: &str = "nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6";

    #[test]
    fn test_part_1() -> () {
        let re = Regex::new(r"(?P<op>nop|acc|jmp) (?P<val>[\+\-]\d+)").unwrap();
        let instructions = INSTRUCTIONS
            .split("\n")
            .map(|line| line_to_instruction(line, &re))
            .collect();
        assert_eq!(run_accumulator(instructions), 5);
    }


    #[test]
    fn test_part_2() -> () {
        let re = Regex::new(r"(?P<op>nop|acc|jmp) (?P<val>[\+\-]\d+)").unwrap();
        let instructions = INSTRUCTIONS
            .split("\n")
            .map(|line| line_to_instruction(line, &re))
            .collect();
        assert_eq!(change_ops(instructions), 8);
    }
}

