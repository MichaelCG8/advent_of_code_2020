use super::shared::{iter_lines, RudolphTheRedNosedResult, SantasLittleError};


#[derive(PartialEq, Clone)]
enum Seat {
    FLOOR,
    EMPTY,
    TAKEN,
}


impl Seat {
    fn value(&self) -> u16 {
        match self {
            Seat::TAKEN => 1,
            _ => 0,
        }
    }
}


type SeatingRow = Vec<Seat>;
type SeatingPlan = Vec<SeatingRow>;
type Kernal = Vec<Vec<u16>>;


fn line_to_seating_row(line: &str) -> RudolphTheRedNosedResult<SeatingRow> {
    Ok(line.trim().chars().map(
        |c| match c {
            '.' => Seat::FLOOR,
            'L' => Seat::EMPTY,
            '#' => Seat::TAKEN,
            _ => panic!(),
        }
    ).collect::<SeatingRow>())
}


fn parse_input() -> RudolphTheRedNosedResult<SeatingPlan> {
    let lines = iter_lines("day11_input.txt").unwrap();
    let seating = lines
        .map(|line| line_to_seating_row(&line.unwrap()).unwrap())
        .collect();
    Ok(seating)
}


fn get_stable_seating(seating: SeatingPlan, vacate_thresh: u16) -> SeatingPlan {
    let mut seating = seating;
    loop {
        let new_seating = convolve(&seating, vacate_thresh);
        if new_seating
            .iter()
            .zip(&seating)
            .all(
                |(ns, s)| ns.iter().zip(s).all(
                    |(inner_ns, inner_s)| inner_ns == inner_s
                )
            ) { return seating }
        seating = new_seating;
    }
}


fn get_stable_seating2(seating: SeatingPlan, vacate_thresh: u16) -> SeatingPlan {
    let mut seating = seating;
    loop {
        let new_seating = progress_step(&seating, vacate_thresh);
        if new_seating
            .iter()
            .zip(&seating)
            .all(
                |(ns, s)| ns.iter().zip(s).all(
                    |(inner_ns, inner_s)| inner_ns == inner_s
                )
            ) { return seating }
        seating = new_seating;
    }
}


struct Kernals;

impl Kernals {
    const BASE:         [[u16; 3]; 3] = [[1, 1, 1],
                                         [1, 0, 1],
                                         [1, 1, 1]];

    const TOP:          [[u16; 3]; 2] = [[1, 0, 1],
                                         [1, 1, 1]];

    const BOTTOM:       [[u16; 3]; 2] = [[1, 1, 1],
                                         [1, 0, 1]];

    const LEFT:         [[u16; 2]; 3] = [[   1, 1],
                                         [   0, 1],
                                         [   1, 1]];

    const RIGHT:        [[u16; 2]; 3] = [[1, 1   ],
                                         [1, 0   ],
                                         [1, 1   ]];

    const TOP_LEFT:     [[u16; 2]; 2] = [[   0, 1],
                                         [   1, 1]];

    const TOP_RIGHT:    [[u16; 2]; 2] = [[1, 0   ],
                                         [1, 1   ]];

    const BOTTOM_LEFT:  [[u16; 2]; 2] = [[   1, 1],
                                         [   0, 1]];

    const BOTTOM_RIGHT: [[u16; 2]; 2] = [[1, 1   ],
                                         [1, 0   ]];
}


fn get_sub_kernal(i: usize, j: usize, max_i: usize, max_j: usize) -> Kernal {
    if i == 0 {
        if j == 0 { return Kernals::TOP_LEFT.iter().map(|row| row.to_vec()).collect() }
        if j == max_j { return Kernals::TOP_RIGHT.iter().map(|row| row.to_vec()).collect() }
        return Kernals::TOP.iter().map(|row| row.to_vec()).collect()
    }
    if i == max_i {
        if j == 0 { return Kernals::BOTTOM_LEFT.iter().map(|row| row.to_vec()).collect() }
        if j == max_j { return Kernals::BOTTOM_RIGHT.iter().map(|row| row.to_vec()).collect() }
        return Kernals::BOTTOM.iter().map(|row| row.to_vec()).collect()
    }
    if j == 0 { return Kernals::LEFT.iter().map(|row| row.to_vec()).collect() }
    if j == max_j { return Kernals::RIGHT.iter().map(|row| row.to_vec()).collect() }
    return Kernals::BASE.iter().map(|row| row.to_vec()).collect()
}


fn get_sub_seating(seating: &SeatingPlan, i: usize, j: usize, max_i: usize, max_j: usize) -> SeatingPlan {
    if i == 0 {
        if j == 0 { return seating[..2].iter().map(|row| row[..2].to_vec()).collect() }
        if j == max_j { return seating[..2].iter().map(|row| row[row.len()-2..].to_vec()).collect() }
        return seating[..2].iter().map(|row| row[j-1..j+2].to_vec()).collect()
    }
    if i == max_i {
        if j == 0 { return seating[seating.len()-2..].iter().map(|row| row[..2].to_vec()).collect() }
        if j == max_j { return seating[seating.len()-2..].iter().map(|row| row[row.len()-2..].to_vec()).collect() }
        return seating[seating.len()-2..].iter().map(|row| row[j-1..j+2].to_vec()).collect()
    }
    if j == 0 { return seating[i-1..i+2].iter().map(|row| row[..2].to_vec()).collect() }
    if j == max_j { return seating[i-1..i+2].iter().map(|row| row[row.len()-2..].to_vec()).collect() }
    return seating[i-1..i+2].iter().map(|row| row[j-1..j+2].to_vec()).collect()
}


fn mat_mul_sum(s: SeatingPlan, k: Kernal) -> u16 {
    let mut total = 0;
    for (s_row, k_row) in s.iter().zip(k) {
        for (seat, kernal_entry) in s_row.iter().zip(k_row) {
            total += seat.value() * kernal_entry;
        }
    }
    total
}


fn convolve(seating: &SeatingPlan, vacate_thresh: u16) -> SeatingPlan {
    let max_i = seating.len() - 1;
    let max_j = seating[0].len() - 1;

    let mut plan = Vec::with_capacity(max_i + 1);
    for i in 0..seating.len() {
        let mut row = Vec::with_capacity(max_j + 1);
        for j in 0..seating[0].len() {
            let seat = &seating[i][j];
            row.push(match seat {
                Seat::FLOOR => Seat::FLOOR,
                _ => {
                    let k = get_sub_kernal(i, j, max_i, max_j);
                    let s = get_sub_seating(seating, i, j, max_i, max_j);
                    let neighbours = mat_mul_sum(s, k);
                    //let neighbours = get_neighbours(seating, i, j, max_i, max_j);
                    let new_seat;
                    if neighbours == 0 { new_seat = Seat::TAKEN }
                    else if neighbours >= vacate_thresh { new_seat = Seat::EMPTY }
                    else { new_seat = seat.clone() }
                    new_seat
                }
            })
        }
        plan.push(row);
    }
    plan
}


fn progress_step(seating: &SeatingPlan, vacate_thresh: u16) -> SeatingPlan {
    let max_i = seating.len() - 1;
    let max_j = seating[0].len() - 1;
    let mut plan = Vec::with_capacity(max_i + 1);
    for i in 0..seating.len() {
        let mut row = Vec::with_capacity(max_j + 1);
        for j in 0..seating[0].len() {
            let seat = &seating[i][j];
            row.push(match seat {
                Seat::FLOOR => Seat::FLOOR,
                _ => {
                    let new_seat;
                    let neighbours = get_visible_seats(seating, i, j, max_i, max_j);
                    if neighbours == 0 { new_seat = Seat::TAKEN }
                    else if neighbours >= vacate_thresh { new_seat = Seat::EMPTY }
                    else { new_seat = seat.clone() }
                    new_seat
                }
            })
        }
        plan.push(row);
    }
    plan
}


fn get_visible_seats(seating: &SeatingPlan, i: usize, j: usize, max_i: usize, max_j: usize) -> u16 {
    let mut visible = 0;

    // Left
    for other_i in (0..i).rev() {
        match seating[other_i][j] {
            Seat::TAKEN => { visible += 1; break },
            Seat::EMPTY => break,
            Seat::FLOOR => (),
        }
    }
    // Right
    for other_i in (i+1)..=max_i {
        match seating[other_i][j] {
            Seat::TAKEN => { visible += 1; break },
            Seat::EMPTY => break,
            Seat::FLOOR => (),
        }
    }
    // Up
    for other_j in (0..j).rev() {
        match seating[i][other_j] {
            Seat::TAKEN => { visible += 1; break },
            Seat::EMPTY => break,
            Seat::FLOOR => (),
        }
    }
    // Down
    for other_j in (j+1)..=max_j {
        match seating[i][other_j] {
            Seat::TAKEN => { visible += 1; break },
            Seat::EMPTY => break,
            Seat::FLOOR => (),
        }
    }
    // Up Left
    for (other_i, other_j) in ((0..i).rev()).zip((0..j).rev()) {
        match seating[other_i][other_j] {
            Seat::TAKEN => { visible += 1; break },
            Seat::EMPTY => break,
            Seat::FLOOR => (),
        }
    }
    // Up Right 
    for (other_i, other_j) in ((0..i).rev()).zip((j+1)..=max_j) {
        match seating[other_i][other_j] {
            Seat::TAKEN => { visible += 1; break },
            Seat::EMPTY => break,
            Seat::FLOOR => (),
        }
    }
    // Down Left
    for (other_i, other_j) in ((i+1)..=max_i).zip((0..j).rev()) {
        match seating[other_i][other_j] {
            Seat::TAKEN => { visible += 1; break },
            Seat::EMPTY => break,
            Seat::FLOOR => (),
        }
    }
    // Down Right
    for (other_i, other_j) in ((i+1)..=max_i).zip((j+1)..=max_j) {
        match seating[other_i][other_j] {
            Seat::TAKEN => { visible += 1; break },
            Seat::EMPTY => break,
            Seat::FLOOR => (),
        }
    }

    visible
}


fn get_neighbours(seating: &SeatingPlan, i: usize, j: usize, max_i: usize, max_j: usize) -> u16 {
    let mut visible = 0;

    // Up
    if i >= 1 && seating[i-1][j] == Seat::TAKEN {
        visible += 1;
    }
    // Down
    if i < max_i && seating[i+1][j] == Seat::TAKEN {
        visible += 1;
    }
    // Left
    if j >= 1 && seating[i][j-1] == Seat::TAKEN {
        visible += 1;
    }
    // Right
    if j < max_j && seating[i][j+1] == Seat::TAKEN {
        visible += 1;
    }
    // Up Left
    if i >= 1 && j >= 1 && seating[i-1][j-1] == Seat::TAKEN {
        visible += 1;
    }
    // Up Right
    if i >= 1 && j < max_j && seating[i-1][j+1] == Seat::TAKEN {
        visible += 1;
    }
    // Down Left
    if i < max_i && j >= 1 && seating[i+1][j-1] == Seat::TAKEN {
        visible += 1;
    }
    // Down Right
    if i < max_i && j < max_j && seating[i+1][j+1] == Seat::TAKEN {
        visible += 1;
    }
    visible
}


fn get_final_filled_seats(seating: SeatingPlan) -> u16 {
    get_stable_seating(seating, 4).iter().map(|row| row.iter().map(|s| s.value()).sum::<u16>()).sum()
}


fn get_final_filled_seats2(seating: SeatingPlan) -> u16 {
    get_stable_seating2(seating, 5).iter().map(|row| row.iter().map(|s| s.value()).sum::<u16>()).sum()
}


pub fn part_1() -> RudolphTheRedNosedResult<u16> {
    let seating = parse_input()?;
    Ok(get_final_filled_seats(seating))
}


pub fn part_2() -> RudolphTheRedNosedResult<u16> {
    let seating = parse_input()?;
    Ok(get_final_filled_seats2(seating))
}

#[cfg(test)]
mod test {
    use super::*;

    const DATA: &str = "#.##.##.##
#######.##
#.#.#..#..
####.##.##
#.##.##.##
#.#####.##
..#.#.....
##########
#.######.#
#.#####.##";

    #[test]
    fn test_part_1() -> () {
        let seating = DATA.split("\n").map(|line| line_to_seating_row(line).unwrap()).collect();
        assert_eq!(get_final_filled_seats(seating), 37);
    }


    #[test]
    fn test_part_2() -> () {
        let seating = DATA.split("\n").map(|line| line_to_seating_row(line).unwrap()).collect();
        assert_eq!(get_final_filled_seats2(seating), 26);
    }
}

