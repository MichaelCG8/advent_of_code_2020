use std::collections::HashMap;

use super::shared::{iter_lines, RudolphTheRedNosedResult, SantasLittleError};


fn parse_input() -> Vec<u16> {
    let lines = iter_lines("day10_input.txt").unwrap();
    let joltages = lines.map(|line| line.unwrap().parse().unwrap()).collect();
    joltages
}


fn joltages_to_gaps(mut joltages: Vec<u16>) -> Vec<u16> {
    joltages.push(0);
    joltages.push(joltages.iter().max().unwrap() + 3);
    joltages.sort();
    let gaps = (0..(joltages.len()-1)).map(|i| joltages[i+1] - joltages[i]).collect();
    gaps
}


fn find_gaps(joltages: Vec<u16>) -> HashMap<u16, u16> {
    let mut count = HashMap::new();
    for gap in joltages_to_gaps(joltages) {
        let gap_count = count.entry(gap).or_insert(0);
        *gap_count += 1;
    }
    count
}

fn find_gaps_prod(joltages: Vec<u16>) -> RudolphTheRedNosedResult<u16> {
    let gaps = find_gaps(joltages);
    let gap1 = match gaps.get(&1) {
        Some(val) => val,
        None => return Err(SantasLittleError(
            format!("Couldn't find key in the gaps map. {}:{}", file!(), line!())
        )),
    };
    let gap3 = match gaps.get(&3) {
        Some(val) => val,
        None => return Err(SantasLittleError(
            format!("Couldn't find key in the gaps map. {}:{}", file!(), line!())
        )),
    };

    Ok(gap1 * gap3)
}


fn paths_to_end(buffer: &Vec<(u16, u64)>, new_joltage: &u16) -> u64 {
    if buffer.is_empty() { return 1 }

    buffer
        .iter()
        .filter(|(joltage, _paths)| joltage - new_joltage <= 3)
        .map(|(_joltage, paths)| paths)
        .sum()
}


fn find_total_paths(mut joltages: Vec<u16>) -> RudolphTheRedNosedResult<u64> {
    joltages.push(0);
    joltages.sort();
    joltages.reverse();
    let mut buffer = Vec::new();
                     
    for (i, joltage) in joltages.iter().enumerate() {
        if *joltage == 0 { return Ok(paths_to_end(&buffer, joltage)) }

        if i <= 2 {
            buffer.push((*joltage, paths_to_end(&buffer, joltage)));
        }
        else {
            buffer[i%3] = (*joltage, paths_to_end(&buffer, joltage));
        }
    }
    Err(SantasLittleError(
        format!("Did not find a joltage equal to 0. {}:{}", file!(), line!())
    ))
}


pub fn part_1() -> RudolphTheRedNosedResult<u16>{
    let joltages = parse_input();
    find_gaps_prod(joltages)
}


pub fn part_2() -> RudolphTheRedNosedResult<u64> {
    let joltages = parse_input();
    find_total_paths(joltages)
}

#[cfg(test)]
mod test {
    use super::*;

    const DATA1: &str = "16
10
15
5
1
11
7
19
6
12
4";

    const DATA2: &str = "28
33
18
42
31
14
46
20
48
47
24
23
49
45
19
38
39
11
1
32
25
35
8
17
7
9
4
2
34
10
3";

    #[test]
    fn test_part_1() -> () {
        let joltages: Vec<u16> = DATA1.split("\n").map(|j| j.parse().unwrap()).collect();
        let mut expected = HashMap::new();
        expected.insert(1, 7);
        expected.insert(3, 5);
        assert_eq!(find_gaps(joltages), expected);

        let joltages: Vec<u16> = DATA2.split("\n").map(|j| j.parse().unwrap()).collect();
        let mut expected = HashMap::new();
        expected.insert(1, 22);
        expected.insert(3, 10);
        assert_eq!(find_gaps(joltages), expected);
    }


    #[test]
    fn test_part_2() -> RudolphTheRedNosedResult<()> {
        let joltages: Vec<u16> = DATA1.split("\n").map(|j| j.parse().unwrap()).collect();
        assert_eq!(find_total_paths(joltages)?, 8);

        let joltages: Vec<u16> = DATA2.split("\n").map(|j| j.parse().unwrap()).collect();
        assert_eq!(find_total_paths(joltages)?, 19208);
        Ok(())
    }
}

