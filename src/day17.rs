use itertools::Itertools;
use ndarray::{Array1, Array2, s};
use super::shared::{iter_lines, RudolphTheRedNosedResult, SantasLittleError};


fn coords_to_index(x: isize, y: isize, z: isize, steps: isize, init_size: (isize, isize)) -> usize {
    let x = x + steps;
    let y = y + steps;
    let z = z + steps;
    (z * (steps * 2 + init_size.0)*(steps * 2 + init_size.1) + y * (steps * 2  + init_size.0) + x) as usize
}


fn coords_to_index_4d(x: isize, y: isize, z: isize, w: isize, steps: isize, init_size: (isize, isize)) -> usize {
    let x = x + steps;
    let y = y + steps;
    let z = z + steps;
    let w = w + steps;
    (
        w * (steps * 2 + init_size.0) * (steps * 2 + init_size.1) * (steps * 2 + 1)
        + z * (steps * 2 + init_size.0) * (steps * 2 + init_size.1)
        + y * (steps * 2  + init_size.0)
        + x
    ) as usize
}


fn neighbours_indices(x: isize, y: isize, z: isize, steps: isize, init_size: (isize, isize)) -> Vec<usize> {
    let x_lo = vec![-steps, x-1].into_iter().max().unwrap();
    let x_hi = vec![x+1, steps + init_size.0 - 1].into_iter().min().unwrap();
    let y_lo = vec![-steps, y-1].into_iter().max().unwrap();
    let y_hi = vec![y+1, steps + init_size.1 - 1].into_iter().min().unwrap();
    let z_lo = vec![-steps, z-1].into_iter().max().unwrap();
    let z_hi = vec![z+1, steps + 1 - 1].into_iter().min().unwrap();
    let a: Vec<usize> = vec![
        x_lo..=x_hi,
        y_lo..=y_hi,
        z_lo..=z_hi,
    ]
        .into_iter()
        .multi_cartesian_product()
        .filter(|xyz| !xyz.iter().all(|&i| i == 0))
        .map(|xyz| coords_to_index(xyz[0], xyz[1], xyz[2], steps, init_size))
        .collect();
    a
}


fn neighbours_indices_4d(x: isize, y: isize, z: isize, w: isize, steps: isize, init_size: (isize, isize)) -> Vec<usize> {
    let x_lo = vec![-steps, x-1].into_iter().max().unwrap();
    let x_hi = vec![x+1, steps + init_size.0 - 1].into_iter().min().unwrap();
    let y_lo = vec![-steps, y-1].into_iter().max().unwrap();
    let y_hi = vec![y+1, steps + init_size.1 - 1].into_iter().min().unwrap();
    let z_lo = vec![-steps, z-1].into_iter().max().unwrap();
    let z_hi = vec![z+1, steps + 1 - 1].into_iter().min().unwrap();
    let w_lo = vec![-steps, w-1].into_iter().max().unwrap();
    let w_hi = vec![w+1, steps + 1 - 1].into_iter().min().unwrap();
    let a: Vec<usize> = vec![
        x_lo..=x_hi,
        y_lo..=y_hi,
        z_lo..=z_hi,
        w_lo..=w_hi,
    ]
        .into_iter()
        .multi_cartesian_product()
        .filter(|xyzw| !xyzw.iter().all(|&i| i == 0))
        .map(|xyzw| coords_to_index_4d(xyzw[0], xyzw[1], xyzw[2], xyzw[3], steps, init_size))
        .collect();
    a
}


fn get_m_v(steps: isize, init_size: (isize, isize)) -> (Array2<usize>, Array1<usize>) {
    let n_locs = ((steps * 2 + init_size.0) * (steps * 2 + init_size.1) * (steps * 2 + 1)) as usize;
    let mut connections = Array2::<usize>::zeros((n_locs, n_locs));
    let contents = Array1::<usize>::zeros(n_locs);;

    for x in -steps..(steps + init_size.0) {
        for y in -steps..(steps + init_size.1) {
            for z in -steps..(steps + 1) {
                let i = coords_to_index(x, y, z, steps, init_size);
                for j in neighbours_indices(x, y, z, steps, init_size) {
                    if i == j { continue }
                    connections[[i, j]] = 1;
                }
            }
        }
    }
    (connections, contents)
}


fn get_m_v_4d(steps: isize, init_size: (isize, isize)) -> (Array2<usize>, Array1<usize>) {
    let n_locs = (
        (steps * 2 + init_size.0) 
        * (steps * 2 + init_size.1) 
        * (steps * 2 + 1)
        * (steps * 2 + 1)
    ) as usize;

    let mut connections = Array2::<usize>::zeros((n_locs, n_locs));
    let contents = Array1::<usize>::zeros(n_locs);;

    for x in -steps..(steps + init_size.0) {
        for y in -steps..(steps + init_size.1) {
            for z in -steps..(steps + 1) {
                for w in -steps..(steps + 1) {
                    let i = coords_to_index_4d(x, y, z, w, steps, init_size);
                    for j in neighbours_indices_4d(x, y, z, w, steps, init_size) {
                        if i == j { continue }
                        connections[[i, j]] = 1;
                    }
                }
            }
        }
    }
    (connections, contents)
}


fn get_total_cubes(mut contents: Array1<usize>, connections: Array2<usize>, steps: isize, init_size: (isize, isize)) -> usize {
    for _ in 0..steps {
        step_forward(&mut contents, &connections, steps, init_size);
        println!("\nTotal:{}", contents.sum());
    }
    contents.sum()
}


fn step_forward(contents: &mut Array1<usize>, connections: &Array2<usize>, steps: isize, init_size: (isize, isize)) -> () {
    let shape = (
        (steps * 2 + 1) as usize,
        (steps * 2 + init_size.1) as usize,
        (steps * 2 + init_size.0) as usize,
    );
//    println!("\n\nContents in\n{:#}", contents.clone().into_shape(shape).unwrap());
    let neighbours = connections.dot(contents);
//    println!("\nNeighbours\n{:#}", neighbours.clone().into_shape(shape).unwrap());

    for (entry, n) in contents.iter_mut().zip(&neighbours) {
        *entry = match entry {
            1 => match n {
                2|3 => 1,
                _ => 0,
            },
            0 => match n {
                3 => 1,
                _ => 0,
            },
            _ => panic!(),
        }
    }
//    println!("\nContents out\n{:#}", contents.clone().into_shape(shape).unwrap());
}


pub fn part_1() -> RudolphTheRedNosedResult<usize>{
    let init_size = (8, 8);
    let data = [
        (0, 5),
        (0, 6),
        (1, 1),
        (1, 2),
        (1, 3),
        (1, 6),
        (1, 7),
        (2, 7),
        (3, 2),
        (3, 4),
        (4, 0),
        (4, 1),
        (5, 1),
        (5, 2),
        (5, 4),
        (5, 7),
        (6, 1),
        (6, 2),
        (6, 3),
        (6, 6),
        (7, 2),
        (7, 4),
        (7, 7),
    ];

    let steps = 6;
    let m_v = get_m_v(steps, init_size);
    let connections = m_v.0;
    let mut contents = m_v.1;

    for (x, y) in data.iter() {
        let idx = coords_to_index(*x, *y, 0, steps, init_size);
        contents[idx] = 1; 
    }
    Ok(get_total_cubes(contents, connections, steps, init_size))
}


pub fn part_2() -> RudolphTheRedNosedResult<usize> {
    let init_size = (8, 8);
    let data = [
        (0, 5),
        (0, 6),
        (1, 1),
        (1, 2),
        (1, 3),
        (1, 6),
        (1, 7),
        (2, 7),
        (3, 2),
        (3, 4),
        (4, 0),
        (4, 1),
        (5, 1),
        (5, 2),
        (5, 4),
        (5, 7),
        (6, 1),
        (6, 2),
        (6, 3),
        (6, 6),
        (7, 2),
        (7, 4),
        (7, 7),
    ];

    let steps = 6;
    let m_v = get_m_v_4d(steps, init_size);
    let connections = m_v.0;
    let mut contents = m_v.1;

    for (x, y) in data.iter() {
        let idx = coords_to_index_4d(*x, *y, 0, 0, steps, init_size);
        contents[idx] = 1; 
    }
    Ok(get_total_cubes(contents, connections, steps, init_size))
}


#[cfg(test)]
mod test {
    use super::*;

    const DATA: [(isize, isize); 5]  = [
        (0, 2),
        (1, 0),
        (1, 2),
        (2, 1),
        (2, 2),
    ];

    #[test]
    fn test_part_1() -> () {
        let init_size = (3, 3);
        for &(steps, result) in [(1, 11), (2, 21), (3, 38), (6, 112)].iter() {
            let m_v = get_m_v(steps, init_size);
            let connections = m_v.0;
            let mut contents = m_v.1;

            for (x, y) in DATA.iter() {
                let idx = coords_to_index(*x, *y, 0, steps, init_size);
                contents[idx] = 1; 
            }
            assert_eq!(get_total_cubes(contents, connections, steps, init_size), result);
            break
        }
    }


    #[test]
    fn test_part_2() -> () {
        let init_size = (3, 3);
        for &(steps, result) in [(1, 29), (2, 60), (6, 848)].iter() {
            let m_v = get_m_v_4d(steps, init_size);
            let connections = m_v.0;
            let mut contents = m_v.1;

            for (x, y) in DATA.iter() {
                let idx = coords_to_index_4d(*x, *y, 0, 0, steps, init_size);
                contents[idx] = 1; 
            }
            assert_eq!(get_total_cubes(contents, connections, steps, init_size), result);
            break
        }
    }
}

