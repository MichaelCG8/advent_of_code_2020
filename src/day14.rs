use std::collections::HashMap;
use std::fs;

use regex::Regex;

use super::shared::{iter_lines, RudolphTheRedNosedResult, SantasLittleError};


#[derive(Clone, Debug)]
struct Operation {
    loc: u64,
    val: u64,
}


#[derive(Clone, Debug)]
struct Step {
    set_mask: u64,
    clear_mask: u64,
    operations: Vec<Operation>,
}


impl Step {
    pub fn new(step: &str) -> Step {
        let mut step = step.trim().split("\n");
        let mask = step.next().unwrap();
        let set_mask = u64::from_str_radix(
            mask.replace("X", "0").as_str(),
            2,
        ).unwrap();
        let clear_mask = u64::from_str_radix(
            mask.replace("X", "1").as_str(),
            2,
        ).unwrap();
        
        let re = Regex::new(r"mem\[(?P<loc>\d+)\] = (?P<val>\d+)$").unwrap();
        let operations = step
            .map(|op| {
                let caps = re.captures(op).unwrap();
                let loc = caps.name("loc").unwrap().as_str().parse().unwrap();
                let val = caps.name("val").unwrap().as_str().parse().unwrap();
                Operation { loc, val }
            })
            .collect();
        Step { set_mask, clear_mask, operations }
    }
}


#[derive(Clone, Debug)]
struct Step2 {
    set_mask: u64,
    floating_mask: u64,
    operations: Vec<Operation>,
}


impl Step2 {
    pub fn new(step: &str) -> Step2 {
        let mut step = step.trim().split("\n");
        let mask = step.next().unwrap();
        let set_mask = u64::from_str_radix(
            mask.replace("X", "0").as_str(),
            2,
        ).unwrap();
        let floating_mask = u64::from_str_radix(
            mask.replace("1", "0").replace("X", "1").as_str(),
            2,
        ).unwrap();
        
        let re = Regex::new(r"mem\[(?P<loc>\d+)\] = (?P<val>\d+)$").unwrap();
        let operations = step
            .map(|op| {
                let caps = re.captures(op).unwrap();
                let loc = caps.name("loc").unwrap().as_str().parse().unwrap();
                let val = caps.name("val").unwrap().as_str().parse().unwrap();
                Operation { loc, val }
            })
            .collect();
        Step2 { set_mask, floating_mask, operations }
    }
}


fn parse_input() -> Vec<Step> {
    fs::read_to_string("day14_input.txt")
        .unwrap()
        .split("mask = ")
        .filter(|&step| step != "")
        .map(|step| Step::new(step))
        .collect()
}


fn parse_input2() -> Vec<Step2> {
    fs::read_to_string("day14_input.txt")
        .unwrap()
        .split("mask = ")
        .filter(|&step| step != "")
        .map(|step| Step2::new(step))
        .collect()
}


fn get_memory(steps: Vec<Step>) -> HashMap<u64, u64> {
    let mut mem = HashMap::new();
    for step in steps {
        for op in step.operations {
            mem.insert(
                op.loc,
                (op.val | step.set_mask) & step.clear_mask,
            );
        }
    }
    mem
}


fn get_memory2(steps: Vec<Step2>) -> HashMap<u64, u64> {
    let mut mem = HashMap::new();
    for step in steps {
        for op in step.operations {
            let loc = op.loc | step.set_mask;
            for l in get_floating_locs(loc, step.floating_mask) {
                mem.insert(l, op.val);
            }
        }
    }
    mem
}


fn get_floating_locs(loc: u64, f_mask: u64) -> Vec<u64> {
    let positions: Vec<usize> = (0..64).filter(|&i| bit_set(f_mask, i)).collect();
    let n_positions = positions.len();
    let n_variants = 2_u64.pow(n_positions as u32);
    let mut locs = Vec::with_capacity(n_variants as usize);
    for variant in 0..n_variants {
        let mut l = loc.clone();
        for i in 0..n_positions {
            if bit_set(variant as u64, i) {
                l |= (1 << positions[i]);
            }
            else {
                l &= !(1 << positions[i]);
            }
        }
        locs.push(l);
    }
    locs
}


fn bit_set(val: u64, pos: usize) -> bool {
    ((val >> pos) & 1) == 1
}


fn get_memory_sum(steps: Vec<Step>) -> u64 {
    let mem = get_memory(steps);
    mem.values().sum()
}


fn get_memory_sum2(steps: Vec<Step2>) -> u64 {
    let mem = get_memory2(steps);
    mem.values().sum()
}


pub fn part_1() -> RudolphTheRedNosedResult<u64>{
    let steps = parse_input();
    Ok(get_memory_sum(steps))
}


pub fn part_2() -> RudolphTheRedNosedResult<u64> {
    let steps = parse_input2();
    Ok(get_memory_sum2(steps))
}

#[cfg(test)]
mod test {
    use super::*;

    const DATA1: &str = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
mem[8] = 11
mem[7] = 101
mem[8] = 0";

    const DATA2_0: &str = "000000000000000000000000000000X1001X
mem[42] = 100";
    const DATA2_1: &str = "00000000000000000000000000000000X0XX
mem[26] = 1";

    #[test]
    fn test_part_1() -> () {
        let steps = vec![Step::new(DATA1)];
        let mut reference = HashMap::new();
        reference.insert(8, 64);
        reference.insert(7, 101);
        assert_eq!(get_memory(steps.clone()), reference);
        assert_eq!(get_memory_sum(steps), 165);
    }


    #[test]
    fn test_part_2() -> () {
        let steps = vec![
            Step2::new(DATA2_0),
            Step2::new(DATA2_1),
        ];
        let mut reference = HashMap::new();
        reference.insert(26, 100);
        reference.insert(27, 100);
        reference.insert(58, 100);
        reference.insert(59, 100);
        reference.insert(16, 1);
        reference.insert(17, 1);
        reference.insert(18, 1);
        reference.insert(19, 1);
        reference.insert(24, 1);
        reference.insert(25, 1);
        reference.insert(26, 1);
        reference.insert(27, 1);
        assert_eq!(get_memory2(steps.clone()), reference);
        assert_eq!(get_memory_sum2(steps), 208)
    }
}

