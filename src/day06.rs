use std::collections::{HashMap, HashSet};
use std::io;

fn parse_input() ->  io::Result<String> {
    let data = std::fs::read_to_string("day06_input.txt")?;
    Ok(data)
}


fn count_group_answers(group: String) -> usize {
    let group = group.replace("\n", "");
    group.chars().collect::<HashSet<_>>().len()
}


fn count_answers_sum(groups: Vec<String>) -> usize {
    groups.into_iter().map(|group| count_group_answers(group)).sum()
}


fn count_group_common_answers(group: String) -> usize {
    let people: Vec<&str> = group.split("\n").collect();
    let n_people = people.len();
    let mut answer_count = HashMap::new();
    for person in people {
        for c in person.chars() {
            answer_count.entry(c).and_modify(|e| *e += 1).or_insert(1);
        }
    }
    let total = answer_count.values().filter(|count| **count == n_people).count();
    total
}


fn count_common_answers_sum(groups: Vec<String>) -> usize {
    groups.into_iter().map(|group| count_group_common_answers(group)).sum()
}

                           
pub fn part_1() -> usize {
    let groups = parse_input().unwrap().trim().split("\n\n").map(|s| s.to_string()).collect();
    count_answers_sum(groups)
}


pub fn part_2() -> usize {
    let groups = parse_input().unwrap().trim().split("\n\n").map(|s| s.to_string()).collect();
    count_common_answers_sum(groups)
}

#[cfg(test)]
mod test {
    use super::*;

    const DATA: &str = "abc

a
b
c

ab
ac

a
a
a
a

b";

    #[test]
    fn test_part_1() -> () {
        let groups: Vec<String> = DATA.split("\n\n").map(|s| s.to_string()).collect();
        assert_eq!(count_group_answers(groups[0].clone()), 3);
        assert_eq!(count_group_answers(groups[1].clone()), 3);
        assert_eq!(count_group_answers(groups[2].clone()), 3);
        assert_eq!(count_group_answers(groups[3].clone()), 1);
        assert_eq!(count_group_answers(groups[4].clone()), 1);
        assert_eq!(count_answers_sum(groups), 11);
    }


    #[test]
    fn test_part_2() -> () {
        let groups: Vec<String> = DATA.split("\n\n").map(|s| s.to_string()).collect();
        assert_eq!(count_group_common_answers(groups[0].clone()), 3);
        assert_eq!(count_group_common_answers(groups[1].clone()), 0);
        assert_eq!(count_group_common_answers(groups[2].clone()), 1);
        assert_eq!(count_group_common_answers(groups[3].clone()), 1);
        assert_eq!(count_group_common_answers(groups[4].clone()), 1);
        assert_eq!(count_common_answers_sum(groups), 6);
    }
}

