use std::fs;

use ndarray::Array2;
use regex::Regex;

use super::shared::{iter_lines, RudolphTheRedNosedResult, SantasLittleError};


#[derive(Debug)]
struct Tile {
    id: usize,
    data: Array2<char>,
}


fn parse_input() -> Vec<Tile> {
    let input = fs::read_to_string("day20_input.txt").unwrap();
    let re = Regex::new(r"Tile (?P<id>\d+):").unwrap();
    input
        .trim()
        .split("\n\n")
        .map(|s| {
            let mut iter = s.split("\n");
            let caps = re.captures(iter.next().unwrap()).unwrap();
            let id = caps.name("id").unwrap().as_str().parse().unwrap();
            let mut data = Vec::new();
            for line in iter {
                data.append(&mut line.chars().collect());
            }
            let data = Array2::from_shape_vec((10, 10), data).unwrap();
            Tile {id, data}
        })
        .collect()
}


pub fn part_1() -> RudolphTheRedNosedResult<String>{
    let input = parse_input();
    println!("{:?}", input);
    Ok("Not yet implemented".to_string())
}


pub fn part_2() -> RudolphTheRedNosedResult<String> {
    parse_input();
    Ok("Not yet implemented".to_string())
}


#[cfg(test)]
mod test {
    use super::*;

    const DATA: &str = "";

    #[test]
    fn test_part_1() -> () {
        let data = DATA;
    }


    #[test]
    fn test_part_2() -> () {
        let data = DATA;
    }
}

