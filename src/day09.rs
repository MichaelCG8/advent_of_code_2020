use itertools::Itertools;

use super::shared::{iter_lines, RudolphTheRedNosedResult};


struct CyclicBuffer<T> {
    pub buffer: Vec<T>,
    pointer: usize,
    full: bool,
    size: usize,
}


impl<T> CyclicBuffer<T> {
    pub fn new(size: usize) -> Self {
        CyclicBuffer {
            buffer: Vec::with_capacity(size),
            pointer: 0,
            full: false,
            size,
        }
    }

    pub fn push(&mut self, item: T) {
        match self.full {
            true => self.buffer[self.pointer] = item,
            false => self.buffer.push(item),
        }
        self.pointer += 1;
        if self.pointer >= self.size {
            self.pointer = 0;
            self.full = true;
        }
    }

    pub fn full(&self) -> bool {
        self.full
    }
}


fn parse_input() -> Vec<u64> {
    let lines = iter_lines("day09_input.txt").unwrap();
    let entries = lines.map(|line| line.unwrap().parse().unwrap()).collect();
    entries
}


fn find_weakness(data: Vec<u64>, bufsize: usize) -> u64 {
    let target = find_invalid_entry(bufsize, &data);
    for buf_len in 2..data.len() {
        for start_pos in 0..(data.len()-buf_len) {
            let slice = &data[start_pos..(start_pos+buf_len)];
            if slice.iter().sum::<u64>() == target {
                return slice.iter().min().unwrap() + slice.iter().max().unwrap()
            }
        }
    }
    panic!();
}


pub fn part_1() -> RudolphTheRedNosedResult<u64>{
    let data = parse_input();
    Ok(find_invalid_entry(25, &data))
}


pub fn part_2() -> RudolphTheRedNosedResult<u64> {
    let data = parse_input();
    Ok(find_weakness(data, 25))
}


fn find_invalid_entry(bufsize: usize, data: &Vec<u64>) -> u64 {
    let mut buf = CyclicBuffer::new(bufsize);
    for value in data {
        if buf.full() {
            let valid_value = buf.buffer.iter()
                .combinations(2)
                .any(|combination| combination.iter().copied().sum::<u64>() == *value);
            if !valid_value {
                return *value
            }
        }
        buf.push(*value);
    }
    panic!();
}


#[cfg(test)]
mod test {
    use super::*;

    const DATA: &str = "35
20
15
25
47
40
62
55
65
95
102
117
150
182
127
219
299
277
309
576";


    #[test]
    fn test_part_1() -> () {
        let data = DATA.split("\n").map(|x| x.parse().unwrap()).collect();
        assert_eq!(find_invalid_entry(5, &data), 127);
    }


    #[test]
    fn test_part_2() -> () {
        let data = DATA.split("\n").map(|x| x.parse().unwrap()).collect();
        assert_eq!(find_weakness(data, 5), 62);
    }
}

