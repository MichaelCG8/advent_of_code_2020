use std::collections::HashMap;
use std::fs;

use regex::Regex;

use super::shared::{iter_lines, RudolphTheRedNosedResult, SantasLittleError};


fn indent(depth: usize) -> String {
    "|   ".repeat(depth)
}


enum RuleMatch<'a> {
    Yes(Vec<&'a str>),
    No,
}


#[derive(Debug)]
enum RuleSetMatch<'a> {
    Yes(Vec<&'a str>),
    No(&'a str),
}


#[derive(Debug)]
struct Rule {
    sub_rules: Option<Vec<Vec<usize>>>,
    value: Option<String>,
}


impl Rule {
    pub fn match_msg<'a>(&self, msg: &'a str, rules: &Rules, depth: usize) -> RuleMatch<'a> {
        if msg.is_empty() {
            //return RuleMatch::No(msg)
            return RuleMatch::No
        }

        if let Some(value) = &self.value {
            if value[..] == msg[..1] {
                return RuleMatch::Yes(vec![&msg[1..]])
            }
            //return RuleMatch::No(msg)
            return RuleMatch::No
        }
        if let Some(sub_rules) = &self.sub_rules {
            let mut matches = Vec::<&str>::new();
            for rule_set in sub_rules {
                let ret = rule_set_matches(&rule_set, msg, rules, depth + 1);
                println!("{}{:?} {:?} {}", indent(depth + 1), rule_set, ret, msg);
                match ret {
                    RuleSetMatch::No(_) => continue,
                    RuleSetMatch::Yes(mut s) => matches.append(&mut s),
                };
            }
            match matches.is_empty() {
                true => return RuleMatch::No,
                false => return RuleMatch::Yes(matches),
            }
        }
        panic!();
    }
}


fn rule_set_matches<'a>(rule_set: &Vec<usize>, msg: &'a str, rules: &Rules, depth: usize) -> RuleSetMatch<'a> {
    let mut msg_copy = vec![msg.clone()];
    for rule_id in rule_set {
        let rule = rules.get(&rule_id).unwrap();
        let mut new_msgs = Vec::new();
        for m in msg_copy {
            println!("{}Matching {} with {} {:?}", indent(depth), m, rule_id, rule);
            let ret = rule.match_msg(m, rules, depth + 1);
            match ret {
                RuleMatch::No => return RuleSetMatch::No(msg),
                RuleMatch::Yes(mut msgs) => new_msgs.append(&mut msgs),
            }
        }
        msg_copy = new_msgs;
    }
    return RuleSetMatch::Yes(msg_copy)
}


type Rules = HashMap<usize, Rule>;


fn parse_rules(s: &str) -> RudolphTheRedNosedResult<Rules> {
    let mut rules = HashMap::new();
    let re1 = Regex::new(r#"(?P<id>\d+): (?P<rest>("a"|"b"|[\d |]+))"#).unwrap();

    for line in s.split("\n") {
        let caps = re1.captures(line).unwrap();
        let id = caps.name("id").unwrap().as_str().parse()?;
        let rest = caps.name("rest").unwrap().as_str();
        match rest {
            "\"a\""|"\"b\"" => {
                let sub_rules = None;
                let value = Some(rest.chars().nth(1).unwrap().to_string());
                rules.insert(id, Rule { sub_rules, value });
            },
            _ => {
                let sub_rules = Some(
                    rest
                    .split(" | ")
                    .map(
                        |rule_set|
                        rule_set.split(" ").map(|r| r.parse().unwrap()).collect()
                    ).collect());
                let value = None;
                rules.insert(id, Rule { sub_rules, value });
            },
        }
    }
    Ok(rules)
}


fn parse_input() -> RudolphTheRedNosedResult<(Rules, Vec<String>)> {
    let input = fs::read_to_string("day19_input.txt").unwrap();
    let mut parts = input.split("\n\n");
    let rules = parse_rules(parts.next().unwrap())?;
    let messages = parts.next().unwrap().split("\n").map(|s| s.to_string()).collect();
    Ok((rules, messages))
}


fn count_matching_messages((rules, messages): (Rules, Vec<String>)) -> usize {
    let rule0 = rules.get(&0).unwrap();
    messages.iter().filter(|m| {
        println!("\nMatching {} with 0 {:?}", m, rule0);
        let ret = rule0.match_msg(m, &rules, 0);
        match ret {
            RuleMatch::Yes(strs) => strs.iter().any(|s| s.is_empty()),
            RuleMatch::No => false,
        }
    }).count()
}


pub fn part_1() -> RudolphTheRedNosedResult<usize>{
    let input = parse_input()?;
    Ok(count_matching_messages(input))
}


pub fn part_2() -> RudolphTheRedNosedResult<String> {
    parse_input();
    Ok("Not yet implemented".to_string())
}


#[cfg(test)]
mod test {
    use super::*;

    const DATA: &str = "0: 4 1 5
1: 2 3 | 3 2
2: 4 4 | 5 5
3: 4 5 | 5 4
4: \"a\"
5: \"b\"

ababbb
bababa
abbbab
aaabbb
aaaabbb";

    const DATA2: &str = "42: 9 14 | 10 1
9: 14 27 | 1 26
10: 23 14 | 28 1
1: \"a\"
11: 42 31
5: 1 14 | 15 1
19: 14 1 | 14 14
12: 24 14 | 19 1
16: 15 1 | 14 14
31: 14 17 | 1 13
6: 14 14 | 1 14
2: 1 24 | 14 4
0: 8 11
13: 14 3 | 1 12
15: 1 | 14
17: 14 2 | 1 7
23: 25 1 | 22 14
28: 16 1
4: 1 1
20: 14 14 | 1 15
3: 5 14 | 16 1
27: 1 6 | 14 18
14: \"b\"
21: 14 1 | 1 14
25: 1 1 | 1 14
22: 14 14
8: 42
26: 14 22 | 1 20
18: 15 15
7: 14 5 | 1 21
24: 14 1

bbabbbbaabaabba";
//babbbbaabbbbbabbbbbbaabaaabaaa
//aaabbbbbbaaaabaababaabababbabaaabbababababaaa
//bbbbbbbaaaabbbbaaabbabaaa
//bbbababbbbaaaaaaaabbababaaababaabab
//ababaaaaaabaaab
//ababaaaaabbbaba
//baabbaaaabbaaaababbaababb
//abbbbabbbbaaaababbbbbbaaaababb
//aaaaabbaabaaaaababaa
//aaaabbaabbaaaaaaabbbabbbaaabbaabaaa
//aabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba";

//bbabbbbaabaabba
//ababaaaaaabaaab
//ababaaaaabbbaba";

//abbbbbabbbaaaababbaabbbbabababbbabbbbbbabaaaa
//bbabbbbaabaabba
//babbbbaabbbbbabbbbbbaabaaabaaa
//aaabbbbbbaaaabaababaabababbabaaabbababababaaa
//bbbbbbbaaaabbbbaaabbabaaa
//bbbababbbbaaaaaaaabbababaaababaabab
//ababaaaaaabaaab
//ababaaaaabbbaba
//baabbaaaabbaaaababbaababb
//abbbbabbbbaaaababbbbbbaaaababb
//aaaaabbaabaaaaababaa
//aaaabbaaaabbaaa
//aaaabbaabbaaaaaaabbbabbbaaabbaabaaa
//babaaabbbaaabaababbaabababaaab
//aabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba";
    
    #[test]
    fn test_part_1() -> RudolphTheRedNosedResult<()> {
        let mut parts = DATA.split("\n\n");
        let rules = parse_rules(parts.next().unwrap())?;
        let messages = parts.next().unwrap().split("\n").map(|s| s.to_string()).collect();
        println!("{:?}\n\n{:?}", rules, messages);
        assert_eq!(count_matching_messages((rules, messages)), 2);
        Ok(())
    }


    #[test]
    fn test_part_2_no_update() -> RudolphTheRedNosedResult<()> {
        let mut parts = DATA2.split("\n\n");
        let rules = parse_rules(parts.next().unwrap())?;
        let messages = parts.next().unwrap().split("\n").map(|s| s.to_string()).collect();
        println!("{:?}\n\n{:?}", rules, messages);
        assert_eq!(count_matching_messages((rules, messages)), 3);
        Ok(())
    }


    #[test]
    fn test_part_2_with_update() -> RudolphTheRedNosedResult<()> {
        let mut parts = DATA2.split("\n\n");
        let mut rules = parse_rules(parts.next().unwrap())?;
        rules.insert(8, Rule {
            sub_rules: Some(vec![vec![42], vec![42, 8]]),
            value: None,
        });
        rules.insert(11, Rule {
            sub_rules: Some(vec![vec![42, 31], vec![42, 11, 31]]),
            value: None,
        });
        let messages = parts.next().unwrap().split("\n").map(|s| s.to_string()).collect();
        println!("{:?}\n\n{:?}", rules, messages);
        assert_eq!(count_matching_messages((rules, messages)), 12);
        
        Ok(())
    }
}

